from binary_node import BinaryNode as Node

n1 = Node(value=2)
n2 = Node(value=5)
n3 = Node(value=1, left=n1, right=n2)
n4 = Node(value=7)
n5 = Node(value=3)
n6 = Node(value=6, left=n4, right=n5)
n7 = Node(value=4, left=n3, right=n6)

graph = n7

'''
Traverse the tree to find the minimum value in the tree
'''
def min_of_tree(graph):
    stack = [graph]
    min_value = graph.value
    while stack:
        this_node = stack.pop()
        if this_node is None:
            break
        if this_node.value < min_value:
            min_value = this_node.value
        if this_node.left:
            stack.append(this_node.left)
        if this_node.right:
            stack.append(this_node.right)
    return min_value

'''
Traverse the tree to find the maximum value in the tree
'''
def max_of_tree(graph):
    stack = [graph]
    max_value = graph.value
    while stack:
        this_node = stack.pop()
        if this_node is None:
            break
        if this_node.value > max_value:
            max_value = this_node.value
        if this_node.left:
            stack.append(this_node.left)
        if this_node.right:
            stack.append(this_node.right)
    return max_value
# Pass these tests
assert min_of_tree(graph) == 1
assert max_of_tree(graph) == 7
